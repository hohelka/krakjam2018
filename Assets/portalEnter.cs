﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalEnter : MonoBehaviour {
    private bool PortalActive = false;
    public GameObject zaslona;
    public GameObject pSystem;
    private bool TransformStart,RevertScaleStart;
    private float SizeSpeed = 0.5f;
    private Vector3 oldScale;
    private Vector3 NewScale;
    private GameObject player;
    // Use this for initialization
    void Start () {
        PortalActive = false;
        TransformStart=false;
        RevertScaleStart = false;
        //zaslona.SetActive(true);
        player = GameObject.FindGameObjectWithTag("Player");
        Debug.Log("Player :" + player.name);
    }

    // Update is called once per frame
    void Update()
    {
        if (PortalActive )
        {
            TransformStart = true;
         
        }
        if (TransformStart)
        {
            zaslona.SetActive(false);
            pSystem.SetActive(true);

            Transform portal2 = GameObject.Find("portal2").transform;
            NewScale = new Vector3(0.1f, 0.1f, 0.1f);
            oldScale = player.transform.localScale;
            player.transform.localScale = Vector3.Lerp(oldScale, NewScale, SizeSpeed);

            if (Vector3.Distance(player.transform.localScale, NewScale) <= 0.1f)
            {
                player.transform.position = portal2.position;
                TransformStart = false;
                RevertScaleStart = true;
            }
        }
            if (RevertScaleStart)
            {
                oldScale = new Vector3(5f, -5f, 5f);
                player.transform.localScale = Vector3.Lerp(NewScale, oldScale, SizeSpeed);
                if (Vector3.Distance(NewScale, oldScale) <= 0.1f)
                {
                    //revert postaci
                    // player.transform.localScale *= -1.0f;
                    Debug.Log("transformation end");
                    RevertScaleStart = false;
                }
            }



        }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            PortalActive = true;
        }
    }
}
