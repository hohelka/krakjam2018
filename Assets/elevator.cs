﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevator : MonoBehaviour {
    
    public  GameObject szafka1;
    public GameObject szafka2;
    private bool[] szafki_status;
    private bool finish = false;
	// Use this for initialization
	void Start () {
        //szafki = GameObject.FindGameObjectsWithTag("szafa");
    }
	
	// Update is called once per frame
	void Update () {
       // szafka1.GetComponent<CollectData>().GetZebranoValue();
       // szafki_status[0] = szafka1.GetComponent<CollectData>().GetZebranoValue();
       // szafki_status[1] = szafka2.GetComponent<CollectData>().GetZebranoValue();
        if (!finish &&szafka1.GetComponent<CollectData>().GetZebranoValue() && szafka2.GetComponent<CollectData>().GetZebranoValue())
        {
            Invoke("openElevator", 1);
            finish = true;
        }
	}
    void openElevator()
    {
        transform.GetComponent<Animator>().SetTrigger("OpenElevator");

    }
}
