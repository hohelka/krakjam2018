﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMission1 : MonoBehaviour {
    public GameObject misja;
     GameObject actualButton;
    public GameObject activeButton;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E))
        {
            
            if (actualButton!=null)
            {
                //actualButton.GetComponent<Animation>().Play();
                bool results = misja.GetComponent<Mission1>().AddCodeLetter(actualButton.name);
                if (results)
                { GameObject.Find("MissionObject1").GetComponent<Mission1>().Win(); }
            }
        }
      
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("button")) {
            if (!collision.gameObject.Equals(actualButton))
            {
                actualButton = null;
                activeButton.GetComponent<Renderer>().enabled = false;

            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("button")) {
            actualButton = collision.gameObject;
            activeButton.transform.position = actualButton.gameObject.transform.position;
            activeButton.GetComponent<Renderer>().enabled = true;
        }
    
    }
}
