﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallChange : MonoBehaviour {
    private GameObject[] wallList;
    private bool PcClose=false;

    // Use this for initialization
    void Start () {
         wallList = GameObject.FindGameObjectsWithTag("wall");
	}
	
	// Update is called once per frame
    void Change()
    {
        foreach (GameObject wall in wallList)
        {



            if (Random.RandomRange(10, 30) > 20)
            {
                Color nowy = wall.transform.GetComponent<SpriteRenderer>().color;

                int randomSign = Random.RandomRange(1, 30);
                int znak = 1;
                if (randomSign % 2 == 0) znak = -1;
                wall.transform.GetComponent<SpriteRenderer>().color = new Color((nowy.r + (Random.RandomRange(-10, 16) * znak)) % 220, (nowy.g + (Random.RandomRange(10, Random.RandomRange(-10, 16) * znak))) % 210, (nowy.b + (Random.RandomRange(10, Random.RandomRange(-10, 16) * znak)) % 230));
            }
            else { continue; }


        }

    }
    void Update () {
        if (PcClose)
        {
            Invoke("Change", 5);
        }
            
        
	}
    private void OnTriggerEnter2D(Collider2D col)
    {
        //Debug.Log("W kolizji z " + col.gameObject.name);
        if (col.gameObject.name == "PC")
        {
            PcClose = true;
        }
    }
}
