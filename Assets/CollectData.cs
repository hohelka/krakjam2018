﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectData : MonoBehaviour {
    private bool szafaActive=false;
    public bool zebrano = false;
    // Use this for initialization
    void Start () {
        transform.GetComponent<Animation>().Play();
	}
	public bool GetZebranoValue()
    { return zebrano; }
	// Update is called once per frame
	void Update () {
       if(szafaActive)
        {
            if (Input.GetKey(KeyCode.E))
            {
              
                zebrano = true;
            }
        }
       if(zebrano)
        {
            transform.GetComponent<Animation>().Stop();

        }
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            szafaActive = true;
        }
    }
}
