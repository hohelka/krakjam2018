﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EnemyInteractableObjectInterface {

	void Action();
	bool getEnabled();
	void setEnabled(bool enabled);

}
