﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTakenController : MonoBehaviour {


	public float speed = 3f;
	Animator robotAnimator;
	new Rigidbody2D rigidbody;
	public bool robotFlipped = false;

    private EnemyInteractableObjectInterface foundEioi;
	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();	
		robotAnimator = GetComponentInChildren<Animator>();
	}
	
	
	// Update is called once per frame
	void Update () {
		var x = Input.GetAxis("Horizontal");

        if (x > 0)
        {
            rigidbody.velocity = transform.right.normalized * speed;
			if (!robotFlipped) {
				transform.Find("Canvas").Find("Selected").localPosition = new Vector3(0f, 0f, 0f); 
            	Rotate();
			}
            SetRunState();
        }

        if (x < 0)
        {
            rigidbody.velocity = -transform.right.normalized * speed;
            if (robotFlipped) {
				transform.Find("Canvas").Find("Selected").localPosition = new Vector3(-0.18f, 0f, 0f); 
            	Rotate();
			}
            SetRunState();
        }

        if (x == 0) {
            SetDefaultState();
        }

        if (Input.GetKeyDown(KeyCode.E)) {
            if (foundEioi != null) {
                foundEioi.Action();
            }
        }
	}


	void SetRunState() {
        if (robotAnimator.isActiveAndEnabled) {
            robotAnimator.SetBool("Run", true);
        }
    }

    void SetDefaultState() {
        rigidbody.velocity = Vector2.zero;
        if (robotAnimator.isActiveAndEnabled) {
            robotAnimator.SetBool("Run", false);
        }

    }
	void Rotate() {
		Transform robotTransform = transform.Find("robot").transform;
        robotTransform.localScale = new Vector2(-robotTransform.localScale.x, robotTransform.localScale.y);
		robotFlipped = !robotFlipped;
    }

    private void OnTriggerEnter2D(Collider2D other) {
            if (foundEioi == null && GetComponent<EnemyTakenController>().enabled) {
                foundEioi = other.GetComponent<EnemyInteractableObjectInterface>();
                if (foundEioi != null) {
                other.transform.GetComponentInChildren<SpriteRenderer>().color = new Color(255f, 255f, 255f, 0.5f);
            }
        }
        
    }

    private void OnTriggerExit2D(Collider2D other) {
        EnemyInteractableObjectInterface eioi = other.GetComponent<EnemyInteractableObjectInterface>();
        if (eioi != null && GetComponent<EnemyTakenController>().enabled && eioi.Equals(foundEioi)) {
            other.transform.GetComponentInChildren<SpriteRenderer>().color = new Color(255f, 255f, 255f, 1f);
            foundEioi = null;
        }
    }
}
