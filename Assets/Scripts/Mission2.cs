﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission2 : MonoBehaviour {

	List<int> leverUsed;
	public void LeverUsed(string name) {
		int lever = System.Int32.Parse(name);
		
		leverUsed.Sort();
		if (leverUsed.Count == 0) {
			if (lever != 1) {
				leverUsed.Clear();
				foreach (EnemyInteractableObjectInterface eioi in GameObject.Find("levers").GetComponentsInChildren<EnemyInteractableObjectInterface>()) {
					eioi.setEnabled(false);
				}
				return;
			} else {
				leverUsed.Add(lever);
				return;
			}
		}
		if (leverUsed[leverUsed.Count - 1] == lever - 1) {
			leverUsed.Add(lever);
		} else {
			leverUsed.Clear();
			foreach (EnemyInteractableObjectInterface eioi in GameObject.Find("levers").GetComponentsInChildren<EnemyInteractableObjectInterface>()) {
				eioi.setEnabled(false);
			}
		}
		if (leverUsed.Count == 5) {
			GameObject.FindGameObjectWithTag("Player").GetComponent<ElevatorPlayerController>().missionComplete = true;
		}
	}
	// Use this for initialization
	void Start () {
		leverUsed = new List<int>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
