﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorPlayerController : MonoBehaviour {

	public GameObject elevatorB;
	public GameObject elevatorA;
	public bool missionComplete = false;
	// Use this for initialization
	void Start () {
		
	}
	 private void OnTriggerEnter2D(Collider2D other) {
		if (missionComplete && other.CompareTag("elevator")) {
			elevatorA = other.gameObject;
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
			transform.GetComponent<PlayerMovementController>().enabled = false;
			other.transform.GetComponent<Animator>().SetTrigger("OpenElevator");
			HidePlayer();
			other.transform.GetComponent<Animator>().SetTrigger("CloseElevator");
			TransferPlayer();
		}
		
		
	}
	private void CloseElevator(){
		//GetComponent<Animation>().Play("close",PlayMode.StopAll);
		
	}

	public void HidePlayer() {
		foreach(SpriteRenderer spriteRenderer in transform.Find("doktorek").GetComponentsInChildren<SpriteRenderer>()) {
			spriteRenderer.color = new Color(255f, 255f, 255f, 0f);
		}
	}

	public void UnHidePlayer() {
		foreach(SpriteRenderer spriteRenderer in transform.Find("doktorek").GetComponentsInChildren<SpriteRenderer>()) {
			spriteRenderer.color = new Color(255f, 255f, 255f, 255f);
		}
	}
	
	void TransferPlayer() {
		transform.position = elevatorB.transform.Find("Position").transform.position;
		transform.GetComponent<PlayerMovementController>().enabled = true;
		elevatorB.GetComponent<Animator>().SetTrigger("OpenElevator");
		UnHidePlayer();
		elevatorB.GetComponent<Animator>().SetTrigger("CloseElevator");
	}
}
