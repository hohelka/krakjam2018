using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PlayerCoverObjectInterface {
    float getX();
    float getY();
    SpriteRenderer GetSpriteRenderer();
}
