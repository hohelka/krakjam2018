﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mission1 : MonoBehaviour {
    private string SecretCode = "ABC";
    private string Code = "";
    public bool startOpening = false;
	// Use this for initialization
	void Start () {
		
	}
	public bool AddCodeLetter(string name)
    {
        switch (name) {
            case "guziki_green":
                {
                    Code+= "A";
                    Debug.Log("Aktualny kod to: " + Code);
                    break; }
            case "guziki_red":
                {
                    Code += "B";
                    Debug.Log("Aktualny kod to: " + Code);
                    break; }
            case "guziki_yellow":
                {
                    Code += "C";
                    Debug.Log("Aktualny kod to: " + Code);
                    break; }
            default:
                break;
            
        }
        
        if (Code.Length == SecretCode.Length)
        {
            if (string.Compare(Code, SecretCode) == 0)
            {
                return true;
            }
            else
            {
                Debug.Log("Mission1 Reset!");
                Code = "";
                return false;
            }
        }
        return false;

    }
    public void Win()
    {
        //otwarcie szlabanu
        Debug.Log("Rozwiazales Mission1!");
        GameObject.Find("szlaban_1").GetComponent<Animation>().Play();
        startOpening = true;
        
    }
    public void Szlaban() {
        GameObject.Find("SzlabanBlocker").GetComponent<BoxCollider2D>().enabled = false;
    }
	// Update is called once per frame
	void Update () {
		if (startOpening && !GameObject.Find("szlaban_1").GetComponent<Animation>().isPlaying) {
            Szlaban();
        }
	}

}
