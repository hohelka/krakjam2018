﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utilities : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnEditEnd() {
		if (transform.Find("Panel").Find("Input").GetComponent<InputField>().text.ToLower().Equals("krakjam")) {
			transform.GetComponentInChildren<Canvas>().enabled = false;
			CancelTransformationPlayer();
			StopEnemy();
		} else {
			transform.Find("Panel").Find("Description").GetComponent<Text>().text = "WRONG! Try one more time!";
		}
	}

	public void CancelTransformationPlayer() {
		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerTransformationController>().TurnOffEnemyControl();
		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerTransformationController>().ReturnToOrginalSprite();
	}

	public void StopEnemy() {
		foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("enemy")) {
			enemy.GetComponent<EnemyController>().enabled = false;
			foreach (SpriteRenderer spriteRenderer in enemy.GetComponentsInChildren<SpriteRenderer>()) {
				spriteRenderer.material.SetColor("_Color", new Color(1f, 1f, 1f, 0.4f));
			}
			GameObject.Find("AnswerBox").GetComponentInChildren<SpriteRenderer>().material.SetColor("_Color", new Color(1f, 1f, 1f, 0.4f));
		}
	}
}
