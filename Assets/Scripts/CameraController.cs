﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	Vector3 offset;	
	Transform target;
	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag("Player").transform;
		offset = target.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = target.position - offset;
	}

	public void changeTarget(Transform target) {
		this.target = target;
	}
}
