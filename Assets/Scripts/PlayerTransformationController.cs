﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PlayerTransformationController : MonoBehaviour {
	// Use this for initialization
	
	public Sprite defaultSprite;
	public List<GameObject> changeSprites;
	private int activeSprite = 0;
	public Canvas spriteChooseMenu;
	public ScrollRect scrollView;
	public GameObject objectImage;
	public bool playerChanged = false;

	public GameObject content;
	private TransmissionController transmissionController;
	private GridLayoutGroup gridLayoutGroup;
	private bool openedDrawer;
	private Transform enemyTransformTaken;
	public int distanceMultiplier;
	void Awake () {
		GetComponentInChildren<particleAttractorLinear>().enabled = false;
		transmissionController = Camera.main.GetComponent<TransmissionController>();
		gridLayoutGroup = scrollView.GetComponentInChildren<GridLayoutGroup>();
		foreach (GameObject sprite in changeSprites) {
			GameObject instantiedButton = (GameObject)Instantiate(objectImage);
			Image image = instantiedButton.GetComponent<Image>();
			image.sprite = sprite.GetComponentInChildren<SpriteRenderer>().sprite;
			image.transform.SetParent(gridLayoutGroup.transform, false);
		}
		SetActiveSprite();
	}

	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Cancel") && playerChanged) {
			ReturnToOrginalSprite();
		}
		if (Input.GetKeyDown(KeyCode.T) && !playerChanged) {
			OpenSpriteChooseMenu();
		}
		if (openedDrawer && Input.GetMouseButtonDown(1) && activeSprite < gridLayoutGroup.transform.GetChildCount()-1) {
			activeSprite++;
			SetActiveSprite();
		}
		if (openedDrawer && Input.GetMouseButtonDown(0) && activeSprite > 0) {
			activeSprite--;
			SetActiveSprite();
		}
		if (openedDrawer && Input.GetButtonDown("Cancel")) {
			CloseSpriteChooseMenu();
		}
		if (openedDrawer && Input.GetKeyDown(KeyCode.E)) {
			Invoke("preTransformToExactSprite", 1.5f);
			GetComponentInChildren<particleAttractorLinear>().target = transform;
			GetComponentInChildren<particleAttractorLinear>().enabled = true;
			SetEmissionRate(0f);
			CloseSpriteChooseMenu();
		}
		CheckDistanceAndChangeParticles();
	}

	void SetActiveSprite() {
		for (int i = 0; i < gridLayoutGroup.transform.GetChildCount(); i++) {
			gridLayoutGroup.transform.GetChild(i).GetComponentsInChildren<Image>()[1].color = new Color(177f, 165f, 225f, 0.0f);
		}
		gridLayoutGroup.transform.GetChild(activeSprite).GetComponentsInChildren<Image>()[1].color = new Color(177f, 165f, 225f, 0.5f);
	}

	void AddListener(Button b, Sprite sprite) 
	{
		b.onClick.AddListener(() => TransformToExactSprite(sprite));
	}

	void CheckDistanceAndChangeParticles() {
		if (enemyTransformTaken != null) {
			float distance = Vector2.Distance(transform.position, enemyTransformTaken.position);
			ParticleSystem.MinMaxCurve emissionRate = SetEmissionRate(distance);
			if (emissionRate.constant <= 0) {
				ReturnToOrginalSprite();
			}
		}
	}

	ParticleSystem.MinMaxCurve SetEmissionRate(float distance) {
		ParticleSystem.EmissionModule emission = GetComponentInChildren<ParticleSystem>().emission;
		emission.rate = 300f - distanceMultiplier*distance;
		return emission.rate;
	}
	void ResetEmissionRate() {
		ParticleSystem.EmissionModule emission = GetComponentInChildren<ParticleSystem>().emission;
		emission.rate = 0;
	}

	void OpenSpriteChooseMenu() {
		spriteChooseMenu.GetComponent<Canvas>().enabled = true;
		openedDrawer = true;
	}

	void CloseSpriteChooseMenu() {
		spriteChooseMenu.GetComponent<Canvas>().enabled = false;
		openedDrawer = false;
	}

	void preTransformToExactSprite() {
		GetComponentInChildren<particleAttractorLinear>().target = null;
	    GetComponentInChildren<particleAttractorLinear>().enabled = false;
		ResetEmissionRate();
		TransformToExactSprite(gridLayoutGroup.transform.GetChild(activeSprite).GetComponent<Image>().sprite);
	}
	void TransformToRandomShape() {
		GetComponent<SpriteRenderer>().sprite = changeSprites[Random.Range(0, changeSprites.Count)].GetComponentInChildren<Sprite>();
	}

	public void ReturnToOrginalSprite() {
		transform.Find("doktorek").gameObject.SetActive(true);
		Camera.main.GetComponent<CameraController>().changeTarget(transform.Find("doktorek"));
		transform.Find("HideObject").GetComponent<SpriteRenderer>().sprite = null;
		transform.Find("HideObject").Find("okulary").gameObject.SetActive(false);
		transform.Find("HideObject").Find("antena").gameObject.SetActive(false);
		GetComponentInChildren<particleAttractorLinear>().enabled = false;
		ResetEmissionRate();
		if (enemyTransformTaken != null) {
			TurnOffEnemyControl();
		}
		playerChanged = false;
	}
	public void TransformToExactSprite(Sprite sprite) {
		transform.Find("doktorek").gameObject.SetActive(false);
		transform.Find("HideObject").localScale = new Vector3(0.2f, 0.2f, 0.2f);
		transform.Find("HideObject").localPosition = new Vector3(0f, 1f, 0f);
		transform.Find("HideObject").Find("okulary").gameObject.SetActive(true);
		transform.Find("HideObject").Find("antena").gameObject.SetActive(true);
		transform.Find("HideObject").GetComponent<SpriteRenderer>().sprite = sprite;
		playerChanged = true;
		CloseSpriteChooseMenu();
	}

	public void SetWavesTarget(Transform transform) {
		GetComponentInChildren<particleAttractorLinear>().target = transform;
		GetComponentInChildren<particleAttractorLinear>().enabled = true;
		Camera.main.GetComponent<CameraController>().changeTarget(transform.parent.parent);
		enemyTransformTaken = transform;
		TurnOnEnemyControl();
	}

	public void TurnOnEnemyControl() {
		bool flipped = !enemyTransformTaken.parent.parent.GetComponent<EnemyController>().robotFlipped;
		enemyTransformTaken.parent.parent.GetComponent<EnemyController>().enabled = false;
		transform.GetComponent<PlayerMovementController>().enabled = false;
		enemyTransformTaken.parent.parent.GetComponent<EnemyTakenController>().robotFlipped = flipped;
		enemyTransformTaken.parent.parent.GetComponent<EnemyTakenController>().enabled = true;
	}

	public void TurnOffEnemyControl() {
		bool flipped = enemyTransformTaken.parent.parent.GetComponent<EnemyTakenController>().robotFlipped;
		enemyTransformTaken.parent.parent.GetComponent<EnemyController>().robotFlipped = !flipped;
		enemyTransformTaken.parent.parent.GetComponent<EnemyController>().enabled = true;
		transform.GetComponent<PlayerMovementController>().enabled = true;
		enemyTransformTaken.parent.parent.GetComponent<EnemyTakenController>().enabled = false;
		enemyTransformTaken = null;
	}
}
