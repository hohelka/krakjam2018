﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyInteractableObjectController : MonoBehaviour, EnemyInteractableObjectInterface {

	private bool enabled = false;

	public Sprite leverOn;
	public Sprite leverOff;
	
    public void Action()
    {
		setEnabled(true);
        GameObject.Find("MissionState").GetComponent<Mission2>().LeverUsed(gameObject.name);
    }

    public bool getEnabled()
    {
        return enabled;
    }
	
	public void setEnabled(bool enabled) {
		if (enabled) {
			Debug.Log("ENABLING!");
			transform.GetComponentInChildren<Animator>().SetTrigger("LeverOn");
		} else {
			transform.GetComponentInChildren<Animator>().SetTrigger("LeverOff");
		}
		this.enabled = enabled;
	}

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
