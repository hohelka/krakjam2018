﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransmissionController : MonoBehaviour {
    private Camera maincamera;
    public float zoomSpeed = 5f;
    public float maxZoomOut = 4.5f;
    public Transform CenterOfMap;
    private float CurrentZoom;
    private Vector3 CurrentTransform;
    private GameObject[] enemies;
    public GameObject point;
    private PlayerTransformationController playerTransformationController;
    private List<Transform> pointList = new List<Transform>();
    private Transform SelectedPoint;
    public void Start()
    {
        playerTransformationController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerTransformationController>();
        AddPoints();

    }
    public void ZoomOut()
    {
        maincamera.transform.position = Vector3.Lerp(maincamera.transform.position, CenterOfMap.transform.position, zoomSpeed);

        maincamera.orthographicSize = Mathf.SmoothStep(CurrentZoom, maxZoomOut, zoomSpeed);
       // Debug.Log("Ide z "+ CurrentTransform.x +" do" + CenterOfMap.transform.position.x) ;

    }
    public void ZoomIn()
    {
        //Debug.Log("Wracam z " + CenterOfMap.transform.position.x + " do " + CurrentTransform.x);
        maincamera.transform.position = Vector3.Lerp(CenterOfMap.transform.position, CurrentTransform, zoomSpeed);

        maincamera.orthographicSize = Mathf.SmoothStep(maxZoomOut, CurrentZoom, zoomSpeed);
    }
    private static int SortByDist(Transform o1, Transform o2)
    {
        float distance1 = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, o1.position);
        float distance2 = Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, o2.position);
        return distance1.CompareTo(distance2);
    }
    public void AddPoints()
    {
        enemies = GameObject.FindGameObjectsWithTag("enemy");

        foreach (GameObject enemy in enemies)
        {

            Transform selectPoint = enemy.transform.Find("Canvas").transform.Find("Selected").transform;

            pointList.Add(selectPoint);

            //Transform pointTranform = enemy.transform;
            //Vector3 movePoint = new Vector3(0.0f, 1.5f, 0.0f);
            //GameObject selectPoint = Instantiate(point, enemy.transform.position, enemy.transform.rotation);
            //selectPoint.transform.position += movePoint;
            //selectPoint.transform.SetParent(enemy.transform);
        }

        pointList.Sort(SortByDist);
        SelectedPoint = pointList[0];
    }
    public void SelectPoint()
    {


        if (Input.GetMouseButtonDown(0))
        {

            int pos = (pointList.IndexOf(SelectedPoint) + 1) % pointList.Count;
            SelectedPoint = pointList[pos];
            Debug.Log("Selected point" + pos);
        }
        if (Input.GetMouseButtonDown(1))
        {

            int pos = (pointList.IndexOf(SelectedPoint) - 1 + pointList.Count) % pointList.Count;
            SelectedPoint = pointList[pos];
            Debug.Log("Selected point" + pos);
        }


        foreach (Transform point in pointList)
        {
            point.GetComponent<Renderer>().enabled = true;
            //wyroznienie punktu
            if (pointList.IndexOf(point) == pointList.IndexOf(SelectedPoint))
            {
               
                point.GetComponent<SpriteRenderer>().color = Color.yellow;
            }
            else
            {
                point.GetComponent<SpriteRenderer>().color = Color.white;

            }
        }
    }
    public void UnselectPoints()
    {
        foreach (Transform point in pointList)
        {
            point.GetComponent<Renderer>().enabled = false;
        }
    }

// Use this for initialization
void Awake () {
        maincamera = Camera.main;
        CurrentZoom=maincamera.orthographicSize;
        CurrentTransform = maincamera.transform.position;

}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("space") && playerTransformationController.playerChanged)
        {
            Debug.Log("Key Presssed");
            ZoomOut();
            SelectPoint();
            if (Input.GetKeyDown(KeyCode.E)) {
                GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<PlayerTransformationController>().SetWavesTarget(SelectedPoint);
            }
        }
        if (Input.GetKeyUp("space") && playerTransformationController.playerChanged)
        {
            Debug.Log("Key Released");
            ZoomIn();
            UnselectPoints();


         }
	}
}
