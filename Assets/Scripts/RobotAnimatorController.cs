﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RobotAnimatorController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void killDoc() {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		player.GetComponentInChildren<Animator>().SetTrigger("Dead");
		foreach (MonoBehaviour mb in player.GetComponents<MonoBehaviour>()) {
			mb.enabled = false;
		}
		Invoke("activeKillingDoc", 5);
	}

	public void activeKillingDoc() {
		
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
