﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    Animator docAnimator;
    public float speed = 4.0f;
    public GameObject player;
    private Rigidbody2D rb;
    private float intensity = 0.9f;
    private bool elevatorNear = false;
    private bool enemyNear = false;
    private bool shelterNear = false;
    private GameObject elevatorRef;
    // Use this for initialization
    void Start()
    {
        docAnimator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody2D>();

        //Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        CheckAction();
        
        var x = Input.GetAxis("Horizontal");
        //Debug.Log("Wart x to :"+ x);

        if (x > 0)
        {
            // rb.AddForce(Vector2.right * Time.deltaTime * speed );
            rb.velocity = transform.right.normalized * speed;
            transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, 0, transform.rotation.w);
            Rotate(false);
            SetRunState();
        }

        if (x < 0)
        {
            rb.velocity = -transform.right.normalized * speed;
            transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, 0, transform.rotation.w);
            Rotate(true);
            SetRunState();
            // rb.AddForce(Vector2.left * Time.deltaTime * speed );
        }

        if (x == 0) {
            SetDefaultState();
        }



    }

    void Rotate(bool flipX) {
        foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>()) {
            spriteRenderer.flipX = flipX;
        }
    }

    void SetRunState() {
        if (docAnimator.isActiveAndEnabled) {
            docAnimator.SetBool("Run", true);
        }
    }

    void SetDefaultState() {
        rb.velocity = Vector2.zero;
        if (docAnimator.isActiveAndEnabled) {
            docAnimator.SetBool("Run", false);
        }

    }
    void UpdateActions(string name, bool value,Collider2D col)
    {
        if (name == "elevator")  //winda collision enter
        {

            elevatorRef = col.gameObject;

            
            elevatorNear = value;
        }
        if (name == "enemy")  //winda collision enter
        {

            enemyNear = value;
        }
        if (name == "shelter")  //winda collision enter
        {

            shelterNear = value;
        }

    }
    void CheckAction()
    {
        if (elevatorNear)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                elevatorRef.GetComponent<ElevatorChange>().TransferPosition(transform);
            }
        }
        if (enemyNear)
        {
        }
        if (shelterNear)
        {
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        UpdateActions(col.gameObject.tag, true,col);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        UpdateActions(col.gameObject.tag, false,col);
    }
}
  