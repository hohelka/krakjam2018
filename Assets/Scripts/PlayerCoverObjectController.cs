﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoverObjectController : MonoBehaviour, PlayerCoverObjectInterface {
    
    public float getX() {
        return 0.2f;
    }
    public float getY() {
        return 0.2f;
    }
    public SpriteRenderer GetSpriteRenderer()
    {
        return transform.GetComponentInChildren<SpriteRenderer>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
