﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerBoazeriaButtonController : MonoBehaviour {
	BoazeriaButtonController boazeriaButtonController;
	List<string> clickedButtons = new List<string>();
	List<string> properList = new List<string>();
	// Use this for initialization
	void Start () {
		properList.Add("5");
		properList.Add("8");
		properList.Add("10");
		properList.Add("11");
		properList.Add("14");
		properList.Add("16");
		properList.Add("19");
		properList.Add("23");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.E)) {
			if (boazeriaButtonController != null) {
				
				boazeriaButtonController.SetActivate();
				
				if (clickedButtons.Contains(boazeriaButtonController.gameObject.name)) {
					clickedButtons.Remove(boazeriaButtonController.gameObject.name);
				} else {
					clickedButtons.Add(boazeriaButtonController.gameObject.name);
				}
				if (properList.SequenceEqual(clickedButtons)) {
					GameObject.Find("Doors").GetComponent<Animator>().SetTrigger("OpenDoors");
					GameObject.Find("DoorBlock").SetActive(false);
				}
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		
		boazeriaButtonController = other.GetComponent<BoazeriaButtonController>();
		
	}

	private void OnTriggerExit2D(Collider2D other) {
		
	}
}
