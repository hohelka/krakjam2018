﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerComputerController : MonoBehaviour {

	Text description;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (description != null && Input.GetKeyDown(KeyCode.E)) {
			Canvas canvas = GameObject.Find("ComputerScreen").GetComponentInChildren<Canvas>();
			canvas.transform.Find("Panel").Find("Description").GetComponentInChildren<Text>().text = description.text;
			canvas.enabled = true;
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		description = other.GetComponentInChildren<Text>();
		other.transform.Find("LoadingScreen").GetComponentInChildren<SpriteRenderer>().enabled = true;
	}

	private void OnTriggerExit2D(Collider2D other) {
		if(other.GetComponentInChildren<Text>().Equals(description)) {
			other.transform.Find("LoadingScreen").GetComponentInChildren<SpriteRenderer>().enabled = false;
			description = null;
			GameObject.Find("ComputerScreen").GetComponentInChildren<Canvas>().enabled = false;
		}
	}
}
