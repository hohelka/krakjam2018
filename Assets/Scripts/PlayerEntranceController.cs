﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerEntranceController : MonoBehaviour {

GameObject entrance;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (entrance != null && Input.GetKeyDown(KeyCode.E)) {
			SceneManager.LoadScene("Scene_2");
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
			if (other.CompareTag("entrance")) {
				Debug.Log(SceneManager.GetActiveScene().name);
				if (SceneManager.GetActiveScene().name.Equals("Scene_1")) {
					entrance = other.gameObject;
				} else if (SceneManager.GetActiveScene().name.Equals("Scene_2")) {
					SceneManager.LoadScene("Scene_3");
				}
			}
		}

	private void OnTriggerExit2D(Collider2D other) {
		
	}
}
