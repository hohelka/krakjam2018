﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoazeriaButtonController : MonoBehaviour {

	public bool activated = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetActivate() {
		activated = !activated;
		Debug.Log(transform.GetComponent<SpriteRenderer>().material.GetColor("_Color"));
		if (activated) {
			if (GetComponentInChildren<ParticleSystem>() != null) {
				TurnOnWaves();
			}
			transform.GetComponent<SpriteRenderer>().material.SetColor("_Color", new Color (255f, 114f, 114f, 255f));
		} else {
			Debug.Log("CLICK ON BOAZERIA");
			if (GetComponentInChildren<ParticleSystem>() != null) {
				TurnOffWaves();
			}
			transform.GetComponent<SpriteRenderer>().material.SetColor("_Color", new Color (1f, 1f, 1f, 1f));
		}
	}

	public void TurnOnWaves() {
		ParticleSystem.EmissionModule emission = GetComponentInChildren<ParticleSystem>().emission;
		emission.rate = 100f;
	}

	public void TurnOffWaves() {
ParticleSystem.EmissionModule emission = GetComponentInChildren<ParticleSystem>().emission;
		emission.rate = 0f;
	}
}
