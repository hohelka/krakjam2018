﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public float speed = 5f;
	public List<Transform> waypoints;
	Animator robotAnimator;
	public bool stopped = false;
	new Rigidbody2D rigidbody;
	bool waiting = false;
	bool spottedPlayer = false;
	Transform player;
	public int dangerLevel = 0;
	private float waitingCounter;
	private float maxWaiting = 3f;
	private float dangerStepDownCounter = 5f;
	public bool dangerDisabled = false;
	public bool robotFlipped;
	int activeGoalPoint;
	public List<Sprite> spritesInRobotSight;

	public float robotPercentAccuracy = 10f;
	// Use this for initialization
	void Start () {
		activeGoalPoint = 0;
		rigidbody = GetComponent<Rigidbody2D>();
		robotAnimator = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (!stopped) {
			if (!spottedPlayer) {
				SetDangerLeverSprite();
				if (!waiting) {
					CheckDistance();
					Vector2 enemyPosition = new Vector2(waypoints[activeGoalPoint].position.x, 0);
					Move(enemyPosition);
				} else {
					StopMoving();
					waitingCounter -= Time.deltaTime;
					if (waitingCounter <= 0) {
						waiting = false;
					}
				}
			} else {
				if (player != null) {
					Vector2 playerPostition = new Vector2(player.position.x, 0);
					Move(playerPostition);
					KillPlayerIfCloseEnough(1f);
				}
			}
		} else {
			StopMoving();
		}
	}

	void SetDangerLeverSprite() {
		CountDownDanger();
		HideAllDangerSprites();
		switch(dangerLevel) {
			case 0:
			break;
			case 1:
			transform.Find("Canvas").transform.Find("info").GetComponent<SpriteRenderer>().enabled = true;
			break;
			case 2:
			transform.Find("Canvas").transform.Find("warn").GetComponent<SpriteRenderer>().enabled = true;
			break;
			case 3:
			transform.Find("Canvas").transform.Find("danger").GetComponent<SpriteRenderer>().enabled = true;
			break;
			default:
			spottedPlayer = true;
			break;
		}
	}

	void CountDownDanger() {
		dangerStepDownCounter -= Time.deltaTime;
		if (dangerStepDownCounter <= 0 && dangerLevel > 0) {
			dangerLevel--;
			dangerStepDownCounter = 5f;
		}
	}
	void HideAllDangerSprites() {
		foreach (SpriteRenderer spriteRenderer in transform.Find("Canvas").GetComponentsInChildren<SpriteRenderer>()) {
			if (spriteRenderer.gameObject.name.Equals("Selected")) {
				continue;
			}
			spriteRenderer.enabled = false;
		}
	}
	void CheckDistance () {
		if (Vector2.Distance(new Vector2(transform.position.x, 0), 
		new Vector2(waypoints[activeGoalPoint].position.x, 0)) < 0.4f) {
			SetWaiting();
			if (activeGoalPoint + 1 == waypoints.Count) {
				activeGoalPoint = 0;
				return;
			}
			activeGoalPoint++;
		}
	}

	void SetWaiting() {
		waiting = true;
		waitingCounter = Random.Range(0, maxWaiting);
	}

	void Move(Vector2 goalPosition) {
		Vector2 direction = Vector2.zero;
		Vector2 delta = goalPosition - new Vector2(transform.position.x, 0);
    	direction = delta/delta.magnitude;
		robotAnimator.SetBool("Run", true);
		if (direction.x > 0) {
			if (robotFlipped) {
				Rotate();
				transform.Find("Canvas").Find("Selected").localPosition = new Vector3(0f, 0f, 0f); 
			};
		} else if (direction.x < 0) {
			if (!robotFlipped) {
				Rotate();
				transform.Find("Canvas").Find("Selected").localPosition = new Vector3(-0.18f, 0f, 0f); 
			};
		}
		rigidbody.velocity = direction * speed;
	}

	void StopMoving() {
		rigidbody.velocity = Vector2.zero;
		robotAnimator.SetBool("Run", false);
	}

	void KillPlayerIfCloseEnough(float distanceToKill) {
		if (Vector2.Distance(new Vector2(transform.position.x, 0), 
		new Vector2(player.position.x, 0)) <= distanceToKill) {
			stopped = true;
			robotAnimator.SetTrigger("RobotAction");
		}
	}

	void Rotate() {
		Transform robotTransform = transform.Find("robot").transform;
        robotTransform.localScale = new Vector2(-robotTransform.localScale.x, robotTransform.localScale.y);
		robotFlipped = !robotFlipped;
    }

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.CompareTag("Player")) {
			float randomNumber = Random.Range(0, 100);
			if (spritesInRobotSight.Contains(other.gameObject.transform.Find("HideObject").GetComponent<SpriteRenderer>().sprite)) {
				if (randomNumber <= robotPercentAccuracy){
					dangerLevel++;
					player = other.gameObject.transform;
					dangerStepDownCounter = 5f;
				}
			} else {
				dangerLevel++;
				player = other.gameObject.transform;
				dangerStepDownCounter = 5f;
			}
		}
		PlayerCoverObjectInterface playerCoverObjectInterface = other.gameObject.GetComponent<PlayerCoverObjectInterface>();
		if (playerCoverObjectInterface != null) {
			spritesInRobotSight.Add(playerCoverObjectInterface.GetSpriteRenderer().sprite);
		}
		if (dangerDisabled) {
			dangerLevel = 4;
		}
	}
	
	private void OnTriggerExit2D(Collider2D other) {
		PlayerCoverObjectInterface playerCoverObjectInterface = other.gameObject.GetComponent<PlayerCoverObjectInterface>();
		if (playerCoverObjectInterface != null) {
			spritesInRobotSight.Remove(playerCoverObjectInterface.GetSpriteRenderer().sprite);
		}
	}
}
